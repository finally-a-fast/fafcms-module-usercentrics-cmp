<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp
 * @see https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp/docs Documentation of fafcms-module-usercentrics-cmp
 * @since File available since Release 1.0.0
 */

namespace fafcms\usercentricscmp;

use Exception;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\base\Event;
use yii\i18n\PhpMessageSource;
use Yii;
use fafcms\fafcms\components\ViewComponent;

/**
 * Class Bootstrap
 * @package fafcms\usercentricscmp
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-usercentrics-cmp';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-usercentrics-cmp'])) {
            $app->i18n->translations['fafcms-usercentrics-cmp'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapFrontendApp(Application $app, PluginModule $module): bool
    {
        try {
            Event::on(ViewComponent::class, ViewComponent::EVENT_END_BODY, static function () use ($module) {
                if (
                    !isset(Yii::$app->fafcms->getDisabledCookieManagerRoutes()[Yii::$app->requestedRoute]) &&
                    !Yii::$app->request->isAjax &&
                    $module->getPluginSettingValue('active')
                ) {
                    Yii::$app->getView()->registerJsFile('https://app.usercentrics.eu/latest/main.js', [
                        'position' => ViewComponent::POS_HEAD,
                        'id' => $module->getPluginSettingValue('id')
                    ]);
                }
            });

            return true;
        } catch (Exception $e) {
            // todo log error
            return false;
        }
    }
}
