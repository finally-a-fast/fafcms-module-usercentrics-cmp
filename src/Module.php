<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp
 * @see https://www.finally-a-fast.com/packages/fafcms-module-usercentrics-cmp/docs Documentation of fafcms-module-usercentrics-cmp
 * @since File available since Release 1.0.0
 */

namespace fafcms\usercentricscmp;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 *
 * @package fafcms\usercentricscmp
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [
                [
                    'active',
                    'id',
                ],
                'required'
            ],
            [
                [
                    'id',
                ],
                'string',
                'max' => '255'
            ],
            [['active'], 'boolean'],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'active',
                'label' => Yii::t('fafcms-usercentrics-cmp', 'active'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'id',
                'label' => Yii::t('fafcms-usercentrics-cmp', 'id'),
                'inputType' => TextInput::class,
            ]),
        ];
    }
}
